#include <stdio.h>
#define MAXLINE 1000

int getline(char line1[],int lim)
{
  int i;
  int c;
    for(i=0; i<lim-1 && (c=getchar())!=EOF && c!='\n' ;++i){
    line1[i] = c;}
  if( c == '\n'){
    line1[i]=c;
    ++i;
  }
  line1[i]='\0';
  return i;}


main()
{
    int i;
    int length;

    char line[MAXLINE];

    while ((length = getline(line, MAXLINE)) > 0) {
        i = length - 2;
        while (i >= 0 && (line[i] == ' ' || line[i] == '\t'))
            --i;
        if (i >= 0) {
            line[i+1] = '\n';
            line[i+2] = '\0';
            printf("%s", line);
        }
    }
    return 0;
}
